#!/usr/bin/env node

const commander = require('commander')

const fs = require('fs')
const AdmZip = require('adm-zip')
const request = require('request')
const mkdirp = require('mkdirp')

const file_url = 'https://gitlab.com/wakenby/pluxury/-/archive/master/pluxury-master.zip'

commander.version('1.0.0').description('rain - сборщик на webpack')

function templateTs(name) {
  return `export function ${camelize(name)} () {\n}\n`
}

function templateScss(name) {
  return `.${name} {\n}\n`
}

function templateHbs(name) {
  return `<div class="${name}"></div>\n`
}

function camelize(str) {
  str = str.replace(/-+/, '-')

  return str
    .split('-')
    .map(
      (word, index) => index === 0 ? word : word[0].toUpperCase() + word.slice(1)
    )
    .join('')
}

function issetBem(name) {
  return fs.existsSync(`src/components/${name}`) || fs.existsSync(`src/pages/${name}`)
}

commander
  .command('init')
  .description('Создание чистого проекта')
  .alias('i')
  .action(() => {
    request.get({url: file_url, encoding: null}, (err, res, body) => {
      const zip = new AdmZip(body)
      const zipEntries = zip.getEntries()

      for (let i = 0; i < zipEntries.length; i++) {
        zipEntries[i].entryName = zipEntries[i].entryName.replace('pluxury-master', '/')
      }

      zip.extractAllTo('', true)
    })
  })

commander
  .command('component <name>')
  .description('Создание компонента')
  .alias('c')
  .action((name) => {
    if (issetBem(name)) return console.log('Bem компонент уже существует')

    mkdirp.sync(`src/components/${name}`)

    fs.writeFileSync(`src/components/${name}/${name}.scss`, templateScss(name))
    fs.writeFileSync(`src/components/${name}/${name}.js`, templateTs(name))
    fs.writeFileSync(`src/components/${name}/${name}.hbs`, templateHbs(name))
  })

commander
  .command('page <name>')
  .description('Создание страницы')
  .alias('p')
  .action((name) => {
    if (issetBem(name)) return console.log('Bem компонент уже существует')

    mkdirp.sync(`src/pages/${name}`)

    fs.writeFileSync(`src/pages/${name}/${name}.scss`, templateScss(name))
    fs.writeFileSync(`src/pages/${name}/${name}.js`, templateTs(name))
    fs.writeFileSync(`src/pages/${name}/${name}.hbs`, templateHbs(name))
  })

commander.parse(process.argv)
